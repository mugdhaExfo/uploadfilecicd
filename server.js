var Express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser');
const fs = require('fs');
const AWS = require('aws-sdk');
const path =  require('path');
const dirname = path.join(__dirname, 'Files');

var app = Express();
app.use(bodyParser.json());

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./Files");
    },
    filename: function(req, file, callback) {
        callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
    }
});

var upload = multer({
    storage: Storage
}).array("imgUploader", 3); //Field name and max count

app.get("/", function(req, res) {
    res.sendFile(__dirname + "/fileUpload.html");
});

app.post("/api/Upload", function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            return res.end("Something went wrong!");
        }
		const files = req.files;
		let uploadFileName = files[0].path;
		console.log(" :: " + files[0].originalname );
		fs.readFile(uploadFileName, (err, data) => {
			if (err) throw err;
			const params = {
				Bucket: 'fleet-training-bucket', // pass your bucket name
				Key: files[0].originalname, 
				Body: JSON.stringify(data, null, 2)
			};
			s3.upload(params, function(s3Err, data) {
				if (s3Err) throw s3Err
				console.log(`File uploaded successfully at ${data.Location}`)
			});
		});
		
		let result = "You have uploaded files: <hr />";
        let index, len;

        // Loop through all the uploaded files and display them on frontend
        for (index = 0, len = files.length; index < len; ++index) {
            result += `<img src="${files[index].path}" width="300" style="margin-right: 20px;">`;
        }
        result += '<hr/><a href="./">Upload more files</a>';
		
        return res.end(result);
    });
});

app.listen(2020, function(a) {
    console.log("Listening to port 2020");
});