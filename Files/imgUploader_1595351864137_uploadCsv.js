const fs = require('fs').promises;
const AWS = require('aws-sdk');

/*const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});*/

/*let s3 = new AWS.S3();

const fileName = 'device.csv';

const uploadFile = () => {
  fs.readFile(fileName, (err, data) => {
     if (err) throw err;
     const params = {
         Bucket: 'fleet-training-bucket', // pass your bucket name
         Key: 'device.csv',
         Body: JSON.stringify(data, null, 2)
     };
     s3.upload(params, function(s3Err, data) {
         if (s3Err) throw s3Err
         console.log(`File uploaded successfully at ${data.Location}`)
     });
  });
};

uploadFile();*/

exports.uploadFile = async (event) => {
    let body = JSON.parse(event.body);
    // TODO implement
    let response = {
        statusCode: 200
    };

    let s3 = new AWS.S3();

    const fileName = 'device.csv';

  try {
    const data = await fs.readFile(fileName);
    const params = {
      Bucket: 'fleet-training-bucket', // pass your bucket name
      Key: 'device.csv',
      Body: JSON.stringify(data, null, 2)
    };
    const response = await s3.upload(params);
    console.log("response.json :: ",response);
  } catch(err) {
    // catches errors both in fetch and response.json
    console.log("response error :: ",response);
    alert(err);
  }
return response.json();
}

/*exports.uploadFile = function (fileName, cb){
  const params = {
    Bucket: 'fleet-training-bucket', // pass your bucket name
    Key: 'device.csv',
    Body: JSON.stringify(data, null, 2)
  };
  const fileName = './device.csv';
  fs.readFile(fileName, (err, cb) => {
    if (err) {
      return cb(err);
    }

    s3.upload(params, function(s3Err, data) {
        if (s3Err) throw s3Err
        console.log(`File uploaded successfully at ${data.Location}`)
    });
 });
}*/

/*module.exports.uploadFile = (event, context, callback) => {
    var s3 = new AWS.S3();

    var params = JSON.parse(event.body);

    var s3Params = {
      Bucket: 'fleet-training-bucket',
      Key:  params.name,
      ContentType: params.type,
      Expires: 3600,
      ACL: 'public-read'
    };
  
    var uploadURL = s3.getSignedUrl('putObject', s3Params);
  
    callback(null, {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      body: JSON.stringify({ uploadURL: uploadURL }),
    })
}*/


/*exports.uploadFile = async (event) => {
    let body = JSON.parse(event.body);
    // TODO implement
    let response = {
        statusCode: 200
    };

     const params = {
         Bucket: 'fleet-training-bucket', // pass your bucket name
         Key: 'device.csv',
         Body: JSON.stringify(data, null, 2)
     };
	 
    let dynamoDb = await uploadCsvFile(params).then(res => res).catch(err => {
        console.log("Error::",err);
        return err;
    });
    response.JSON.stringify(dynamoDb);
    return response;
};

function uploadCsvFile (params) {
	let s3 = new AWS.S3();
	const fileName = 'device.csv';
    return new Promise((resolve, reject) => {
		  fs.readFile(fileName, (err, data) => {
		    if (err) {
                reject(err);
            }
            else {    
				s3.upload(params, function(s3Err, data) {
					 if (s3Err) throw s3Err
					 resolve(data);
					 console.log(`File uploaded successfully at ${data.Location}`)
				 });
				}
		  });
    });
}*/
